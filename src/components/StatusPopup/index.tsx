import { useTheme, scale, Layout, typography } from '@scripts/gds';
import Popup, { PopupProps } from '@controls/Popup';

import DeleteIcon from '@icons/20/delete.svg';
import WarningIcon from '@icons/20/warning.svg';
import CheckCircleIcon from '@icons/20/checkCircle.svg';

enum StatusEnum {
    SUCCESS = 'success',
    WARNING = 'warning',
    DELETE = 'delete',
}

interface StatusPopupProps extends PopupProps {
    status?: string;
}

const StatusPopup = ({ status = StatusEnum.SUCCESS, title, children, ...props }: StatusPopupProps) => {
    const { colors } = useTheme();
    return (
        <Popup isCloseButton={false} {...props}>
            {/* TODO: Ставлю паддинги пока-что через css, если тенденция на них сохранится, то заменить в Body */}
            <Popup.Body css={{ padding: `${scale(5, true)}px ${scale(2)}px` }}>
                <Layout cols={[`${scale(5, true)}px`, 1]} gap={scale(2)}>
                    <Layout.Item>
                        {status === StatusEnum.SUCCESS && <CheckCircleIcon css={{ fill: colors?.success }} />}
                        {status === StatusEnum.WARNING && <WarningIcon css={{ fill: colors?.warning }} />}
                        {status === StatusEnum.DELETE && <DeleteIcon css={{ fill: colors?.danger }} />}
                    </Layout.Item>
                    <Layout.Item>
                        <p css={{ marginBottom: scale(1, true), ...typography('h3') }}>{title}</p>
                        {children}
                    </Layout.Item>
                </Layout>
            </Popup.Body>
        </Popup>
    );
};

export default StatusPopup;
