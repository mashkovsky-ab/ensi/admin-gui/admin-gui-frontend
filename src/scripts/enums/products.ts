export enum ImageTypes {
    BASE = 1,
    CATALOG,
    GALLERY,
    DESCRIPTION,
}

export enum ProductStatePopup {
    SAVED = 'saved',
    DELETE = 'delete',
    DELETE_SUCCESS = 'deleteSuccess',
    LEAVE = 'leave',
}
