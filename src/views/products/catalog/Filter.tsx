import Block from '@components/Block';
import Form from '@components/controls/Form';
import { Button, scale, Layout } from '@scripts/gds';
import Select from '@components/controls/Select';

import { FormikValues } from 'formik';
import CalendarRange from '@components/controls/CalendarRange';
import { useMemo } from 'react';
// import FiltersIcon from '@icons/small/filters.svg';
// import MultiSelect from '@components/controls/MultiSelect';
/* import { prepareForSelectFromObject } from '@scripts/helpers'; */

const CatalogFilter = ({
    initialValues,
    onSubmit,
    onReset,
    apiTypes,
}: {
    initialValues: FormikValues;
    onSubmit: (filters: FormikValues) => void;
    onReset: () => void;
    apiTypes: any;
}) => {
    /* const adultValues = {
        false: 'Нет',
        true: 'Да',
        null: 'Все'
    } */

    /* const adults = useMemo(() => prepareForSelectFromObject(adultValues), []); */

    const typesOptions = useMemo(
        () => apiTypes?.data.map(({ id, name }: { id: number; name: string }) => ({ value: id, label: name })) || [],
        [apiTypes?.data]
    );

    return (
        <>
            <Block css={{ marginBottom: scale(3) }}>
                <Form initialValues={initialValues} enableReinitialize onSubmit={onSubmit}>
                    <Block.Body>
                        <Layout cols={12} css={{ marginBottom: scale(3) }}>
                            <Layout.Item col={4}>
                                <Form.Field name="id" label="ID" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="name" label="Название товара" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="code" label="Код товара" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="vendor_code" label="Артикул" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="type">
                                    <Select label="Тип товара" items={[...typesOptions]} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="is_adult">
                                    <Select
                                        label="Товар 18+"
                                        defaultIndex={0}
                                        items={[
                                            { value: '', label: 'Все' },
                                            { value: '1', label: 'Да' },
                                            { value: '0', label: 'Нет' },
                                        ]}
                                    />
                                </Form.Field>
                            </Layout.Item>
                            {/* <Layout.Item col={4} >
                                <div css={{ display: 'flex', justifyContent: 'space-between', columnGap: scale(1) }} >
                                    <Form.Field name="priceFrom" label="Стоимость" type="number" placeholder="От" Icon={RoubleIcon} />
                                    <Form.Field name="priceTo" type="number" placeholder="До" css={{ marginTop: scale(7, true) }} Icon={RoubleIcon} />
                                </div>
                            </Layout.Item> */}
                            <Layout.Item col={4}>
                                <Form.Field name="allow_publish">
                                    <Select
                                        label="Активность на витрине"
                                        items={[
                                            { value: '', label: 'Все' },
                                            { value: '1', label: 'Активен' },
                                            { value: '0', label: 'Не активен' },
                                        ]}
                                    />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <CalendarRange
                                    label="Дата создания"
                                    nameFrom="created_at_gte"
                                    nameTo="created_at_lte"
                                />
                            </Layout.Item>
                            {/* <Layout.Item col={4}>
                                <CalendarRange
                                    label="Дата обновления"
                                    nameTo="updated_at_lte"
                                    nameFrom="updated_at_gte"
                                />
                            </Layout.Item> */}
                        </Layout>
                        <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                            {/* <Button theme="secondary" type="button" Icon={FiltersIcon} >
                                Настроить фильтры
                            </Button> */}
                            <div css={{ marginLeft: 'auto' }}>
                                <Form.Reset
                                    theme="secondary"
                                    css={{ marginLeft: scale(2) }}
                                    type="button"
                                    onClick={onReset}
                                >
                                    Сбросить
                                </Form.Reset>
                                <Button theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </div>
                    </Block.Body>
                </Form>
            </Block>
        </>
    );
};

export default CatalogFilter;
