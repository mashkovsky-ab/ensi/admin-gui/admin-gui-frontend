import { Layout, scale, useTheme, typography } from '@scripts/gds';

import Textarea from '@controls/Textarea';
import Dropzone from '@controls/Dropzone';
import Form from '@controls/Form';

interface ContentProps {
    removedImages: string[];
    setRemovedImages: (urls: string[]) => void;
}

const Content = ({ removedImages, setRemovedImages }: ContentProps) => {
    const { colors } = useTheme();
    return (
        <>
            <Form.Field name="description" label="Описание">
                <Textarea />
            </Form.Field>
            <div
                css={{
                    marginTop: scale(2),
                    marginBottom: scale(2),
                    height: '1px',
                    background: colors?.grey300,
                }}
            />
            <Layout cols={12}>
                <Layout.Item col={9}>
                    <Form.Field label="Изображения" name="images">
                        <Dropzone
                            accept={['image/jpeg', 'image/jpg', 'image/png']}
                            onFileRemove={(_, file) => {
                                if (removedImages.indexOf(file.name) === -1) {
                                    setRemovedImages([...removedImages, file.name]);
                                }
                            }}
                        />
                    </Form.Field>
                </Layout.Item>
                <Layout.Item col={3} css={{ minWidth: '225px' }}>
                    <h5
                        css={{
                            ...typography('bodySmBold'),
                            color: colors?.grey700,
                            marginTop: scale(2),
                            marginBottom: scale(1),
                        }}
                    >
                        Требования к файлу:
                    </h5>
                    <ul
                        css={{
                            ...typography('bodySm'),
                            color: colors?.grey800,
                        }}
                    >
                        <li>1. Максимальное кол-во &#8212; 10шт;</li>
                        <li>2. Формат &#8212; JPEG или PNG;</li>
                        <li>3. Размер &#8212; не больше 10 МБ;</li>
                    </ul>
                </Layout.Item>
            </Layout>
        </>
    );
};
export default Content;
