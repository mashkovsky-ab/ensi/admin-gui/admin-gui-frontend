import { colors, Layout, typography } from '@scripts/gds';
import { useMemo } from 'react';
import { useProductsTypes } from '@api/catalog/products';

import Form from '@components/controls/Form';
import Checkbox from '@components/controls/Checkbox';
import Select from '@components/controls/Select';

const MainData = () => {
    const { data: apiTypes } = useProductsTypes();
    const porductTypes = useMemo(
        () => (apiTypes ? apiTypes.data.map(el => ({ value: el.id, label: el.name })) : []),
        [apiTypes]
    );

    return (
        <>
            <Layout cols={12}>
                <Layout.Item col={6}>
                    <Form.Field name="name" label="Название товара" />
                </Layout.Item>
                <Layout.Item col={6}>
                    <Form.Field name="vendor_code" label="Артикул" />
                </Layout.Item>
                <Layout.Item col={6}>
                    <Form.Field name="barcode" type="number" label="Штрихкод" />
                </Layout.Item>
                <Layout.Item col={6}>
                    <Form.Field name="base_price" type="number" label="Стоимость" />
                </Layout.Item>
                <Layout.Item col={6}>
                    <Form.Field name="type" label="Тип товара">
                        <Select items={[...porductTypes]} />
                    </Form.Field>
                </Layout.Item>
                <Layout.Item col={12}>
                    <Form.Field name="is_adult">
                        <Checkbox>Товар 18+</Checkbox>
                    </Form.Field>
                </Layout.Item>
                <Layout.Item col={12}>
                    <hr />
                </Layout.Item>
                <Layout.Item col={12}>
                    <h4
                        css={{
                            ...typography('bodyMdBold'),
                            color: colors?.grey700,
                        }}
                    >
                        Габариты
                    </h4>
                </Layout.Item>
                <Layout.Item col={4}>
                    <Form.Field name="width" type="number" label="Ширина" />
                </Layout.Item>
                <Layout.Item col={4}>
                    <Form.Field name="height" type="number" label="Высота" />
                </Layout.Item>
                <Layout.Item col={4}>
                    <Form.Field name="length" type="number" label="Длина" />
                </Layout.Item>
                <Layout.Item col={6}>
                    <Form.Field name="weight_gross" type="number" label="Масса, кг" />
                </Layout.Item>
                <Layout.Item col={6}>
                    <Form.Field name="weight" type="number" label="Масса нетто, кг" />
                </Layout.Item>
            </Layout>
        </>
    );
};

export default MainData;
