import { useState, useMemo, ReactNode, useReducer, useEffect, useCallback } from 'react';
import { useRouter } from 'next/router';
import { CSSObject } from '@emotion/core';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import {
    useProductDetail,
    useProductDetailCreate,
    useProductDetailDelete,
    useProductDetailUpdate,
    useProductPreloadImage,
    useProductUpdateAllImages,
    useProductUpdateSeveralImages,
} from '@api/catalog/products';

import { useError, useSuccess } from '@context/modal';
import { Button, colors, Layout, scale, typography } from '@scripts/gds';
import { useTabs } from '@scripts/hooks';
import { ModalMessages } from '@scripts/constants';

import Tabs from '@components/controls/Tabs';
import Form from '@components/controls/Form';
import Checkbox from '@components/controls/Checkbox';
import CopyButton from '@components/CopyButton';
import { downloadFile } from '@components/controls/Dropzone/utils';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import { ProductStatePopup } from '@scripts/enums';
import StatusPopup from '@components/StatusPopup';
import CheckIcon from '@icons/small/check.svg';

import MainData from './MainData';
import Content from './Content';
import useForm from './useForm';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

interface PopupDataItem {
    action: ProductStatePopup;
    title: string;
    description?: string;
    status: string;
}

interface ImgData {
    url: string;
    name: string;
}

const POPUP_DATA: PopupDataItem[] = [
    {
        action: ProductStatePopup.DELETE_SUCCESS,
        title: 'Успех',
        description: 'Товар удален из системы',
        status: 'success',
    },
    {
        action: ProductStatePopup.DELETE,
        title: 'Удаление',
        description: 'Товар будет удален из системы безвозвратно.',
        status: 'delete',
    },
    {
        action: ProductStatePopup.LEAVE,
        title: 'Покинуть форму редактирования?',
        description: 'Внесенные изменения не будут сохранены.',
        status: 'warning',
    },
    {
        action: ProductStatePopup.SAVED,
        title: 'Товар создан',
        status: 'success',
    },
];

const Product = () => {
    const { query, push } = useRouter();
    const { index: tabIndex, getTabsProps } = useTabs();

    const [newID, setNewID] = useState(0);

    const popupReducerFunc = (
        state: { open: boolean; popupAction?: ProductStatePopup },
        action: { open?: boolean; popupAction?: ProductStatePopup }
    ) => ({ ...state, ...action });

    const [popupReducer, setPopupReducer] = useReducer(popupReducerFunc, {
        open: false,
    });

    const popupData = useMemo(
        () => popupReducer.popupAction && POPUP_DATA.find(d => d.action === popupReducer.popupAction),
        [popupReducer.popupAction]
    );

    const { id: queryId } = query;

    const id = useMemo(() => Number(queryId), [queryId]);

    const isNewProduct = useMemo(() => queryId === 'new', [queryId]);

    const productUpdate = useProductDetailUpdate();
    const productCreate = useProductDetailCreate();
    const productDelete = useProductDetailDelete();

    const loadImagesOnServer = useProductPreloadImage();
    const productImagesAdd = useProductUpdateAllImages();
    const productImagesUpdate = useProductUpdateSeveralImages();

    const {
        data: apiData,
        error,
        isLoading,
    } = useProductDetail(
        {
            id: String(id),
            include: 'images',
        },
        Boolean(id) && !isNewProduct
    );

    const productData = useMemo(() => apiData?.data, [apiData]);

    const [imagesState, setImagesState] = useState<File[]>([]);
    const [removedImages, setRemovedImages] = useState<string[]>([]);

    const getImages = useCallback(async () => {
        if (productData?.images?.length) {
            await Promise.all(productData.images.map(img => downloadFile(img.url, img.name))).then(res => {
                const filtredFiles = res.reduce((acc, file) => (file ? [...acc, file] : acc), [] as File[]);
                setImagesState(filtredFiles);
            });
        }
    }, [productData?.images]);

    useEffect(() => {
        getImages();
    }, [getImages]);

    const { initialValues, validationSchema, getFormValues } = useForm({ productData, images: imagesState });

    useError(error || productUpdate.error || productCreate.error || productDelete.error || productImagesAdd.error);
    useSuccess(productUpdate.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(loadImagesOnServer.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');

    const topButtonStyles: CSSObject = {
        display: 'flex',
        position: 'absolute',
        top: scale(1, true),
        right: isNewProduct ? scale(5) : scale(3),
        padding: scale(1),
        background: colors?.white,
    };

    const loadImg = async (img: File) => {
        const formData = new FormData();
        formData.append('file', img);
        return loadImagesOnServer.mutateAsync({ formData });
    };

    const loadNewImgs = (imgs: File[], method: (img: ImgData[]) => void) => {
        Promise.all(imgs.map(img => loadImg(img))).then(imgRespons => {
            const temp = imgRespons.reduce((acc, imgFile, index) => {
                const img = imgs.find(i => i.name === imgs[index].name);
                if (img) {
                    acc.push({
                        url: imgFile.data.url,
                        name: img.name,
                    });
                }
                return acc;
            }, [] as ImgData[]);
            method(temp);
        });
    };

    const addProductImgs = async (values: typeof initialValues, images: File[], idParam: number) => {
        await loadNewImgs(images, imgs => {
            const startSortValue = values.images.length - imgs.length;
            const parsedImages = imgs.map((img, index) => ({ ...img, sort: startSortValue + index }));
            productImagesUpdate.mutateAsync({ id: idParam, imgs: parsedImages });
        });
    };

    const updateProductImgs = async (values: typeof initialValues, idParam?: number) => {
        const productId = idParam || id;
        if (!values.images.length) {
            productImagesAdd.mutateAsync({ id: productId, imgs: [] });
            setRemovedImages([]);
            return;
        }

        // Картинки, которые были изначально
        const remainingImages = productData?.images?.filter(img => {
            if (img.name) {
                return removedImages.indexOf(img.name) === -1;
            }
            return true;
        });

        // Новые картинки
        const newImages = values.images.filter(
            img => remainingImages?.findIndex(remainingImg => remainingImg.name === img.name) === -1
        );

        const isSortingSaved = values.images.every((img, index) => {
            const findedIndex = productData?.images?.findIndex(productImg => productImg.name === img.name);
            if (findedIndex === -1) {
                return true;
            }
            return findedIndex === index;
        });

        if (removedImages.length || !isSortingSaved) {
            await loadNewImgs(newImages, imgs => {
                const allImg = remainingImages ? [...imgs, ...remainingImages] : imgs;
                const parsedImages = values.images.reduce(
                    (acc, imgFile) => {
                        if (allImg.length) {
                            const img = allImg.find(rImg => rImg.name === imgFile.name);
                            if (img) {
                                acc.push({
                                    url: img.url,
                                    name: img.name,
                                    sort: acc.length,
                                });
                            }
                        }
                        return acc;
                    },
                    [] as {
                        url?: string;
                        name?: string;
                        sort?: number;
                    }[]
                );
                productImagesAdd.mutateAsync({ id: productId, imgs: parsedImages });
            });
            setRemovedImages([]);
            return;
        }

        if (
            (productData?.images && values.images.length && values.images.length > productData.images.length) ||
            !productData?.images?.length
        ) {
            await addProductImgs(values, newImages, productId);
        }
    };

    const createProduct = async (values: typeof initialValues) => {
        const product = await productCreate.mutateAsync(getFormValues(values));
        const newId = product.data.id;
        await addProductImgs(values, values.images, newId);
        setNewID(newId);
        setPopupReducer({ open: true, popupAction: ProductStatePopup.SAVED });
    };
    const updateProduct = async (values: typeof initialValues) => {
        await updateProductImgs(values);
        await productUpdate.mutateAsync({
            id,
            data: getFormValues(values),
        });
    };

    const statePopupFooterCSS: CSSObject = {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: scale(3),
        button: {
            ':not(first-child)': {
                marginLeft: scale(1),
            },
        },
    };

    return (
        <>
            <PageWrapper
                // eslint-disable-next-line no-nested-ternary
                h1={queryId === 'new' ? 'Новый товар' : productData ? productData.name : ''}
                css={{ position: 'relative' }}
                isLoading={
                    isLoading ||
                    productUpdate.isLoading ||
                    productCreate.isLoading ||
                    productDelete.isLoading ||
                    productImagesAdd.isLoading
                }
            >
                <Form
                    initialValues={initialValues}
                    enableReinitialize
                    validationSchema={validationSchema}
                    onSubmit={async values => {
                        if (isNewProduct) {
                            createProduct(values);
                            return;
                        }
                        updateProduct(values);
                    }}
                >
                    <>
                        <div css={{ ...topButtonStyles }}>
                            {!isNewProduct && (
                                <Button
                                    onClick={() =>
                                        setPopupReducer({ open: true, popupAction: ProductStatePopup.DELETE })
                                    }
                                    theme="dangerous"
                                    css={{ marginRight: scale(1) }}
                                >
                                    Удалить
                                </Button>
                            )}
                            <Button type="submit" Icon={CheckIcon} iconAfter>
                                Сохранить
                            </Button>
                        </div>
                        <div css={{ display: 'flex' }}>
                            <div css={{ width: '100%', marginRight: scale(2) }}>
                                <Tabs {...getTabsProps()}>
                                    <Tabs.List>
                                        <Tabs.Tab>Основные данные</Tabs.Tab>
                                        <Tabs.Tab>Контент</Tabs.Tab>
                                    </Tabs.List>
                                    <Block css={{ padding: scale(3), ...(!tabIndex && { borderTopLeftRadius: 0 }) }}>
                                        <Tabs.Panel>
                                            <MainData />
                                        </Tabs.Panel>
                                        <Tabs.Panel>
                                            <Content
                                                removedImages={removedImages}
                                                setRemovedImages={setRemovedImages}
                                            />
                                        </Tabs.Panel>
                                    </Block>
                                </Tabs>
                            </div>
                            <Block
                                css={{
                                    maxWidth: scale(38),
                                    height: '100%',
                                    marginTop: scale(6),
                                    padding: scale(3),
                                    position: 'sticky',
                                    top: scale(1),
                                }}
                            >
                                <Layout cols={1} gap={scale(3)}>
                                    <Layout.Item>
                                        <Form.Field name="allow_publish">
                                            <Checkbox value="allow_publish">Активность</Checkbox>
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item>
                                        <SideBarItem name="ID">
                                            {id ? <CopyButton>{String(id)}</CopyButton> : null}
                                        </SideBarItem>
                                        <SideBarItem name="Код">
                                            {initialValues.code ? <CopyButton>{initialValues.code}</CopyButton> : null}
                                        </SideBarItem>
                                    </Layout.Item>
                                    <Layout.Item>
                                        <SideBarItem name="Изменен">
                                            {initialValues.updated_at
                                                ? format(new Date(initialValues.updated_at), 'dd.MM.yyyy p', {
                                                      locale: ru,
                                                  })
                                                : null}
                                        </SideBarItem>
                                        <SideBarItem name="Создан">
                                            {initialValues.created_at
                                                ? format(new Date(initialValues.created_at), 'dd.MM.yyyy p', {
                                                      locale: ru,
                                                  })
                                                : null}
                                        </SideBarItem>
                                    </Layout.Item>
                                </Layout>
                            </Block>
                        </div>
                    </>
                </Form>
                <StatusPopup
                    isOpen={popupReducer.open}
                    onRequestClose={() => setPopupReducer({ open: false })}
                    id="product-state-popup"
                    title={popupData?.title}
                    status={popupData?.status}
                    onAfterClose={() => {
                        setPopupReducer({ popupAction: undefined });
                    }}
                    css={{ width: scale(50) }}
                >
                    {popupData?.description && (
                        <p css={{ ...typography('bodyMd'), marginTop: scale(1) }}>{popupData.description}</p>
                    )}

                    {popupReducer.popupAction === ProductStatePopup.SAVED && (
                        <div css={statePopupFooterCSS}>
                            <Button onClick={() => setPopupReducer({ open: false })} theme="secondary">
                                Остаться
                            </Button>
                            <Button
                                theme="primary"
                                onClick={() => {
                                    push(`/products/catalog/${newID}`);
                                    setPopupReducer({ open: false });
                                }}
                            >
                                Перейти к товару
                            </Button>
                        </div>
                    )}
                    {popupReducer.popupAction === ProductStatePopup.DELETE && (
                        <div css={statePopupFooterCSS}>
                            <Button onClick={() => setPopupReducer({ open: false })} theme="secondary">
                                Отменить
                            </Button>
                            <Button
                                onClick={async () => {
                                    setPopupReducer({ open: false });
                                    await productDelete.mutateAsync(Number(id), {
                                        onSuccess: () => {
                                            setPopupReducer({
                                                open: true,
                                                popupAction: ProductStatePopup.DELETE_SUCCESS,
                                            });
                                            setTimeout(() => {
                                                push(`/products/catalog`);
                                            }, 1000);
                                        },
                                    });
                                }}
                                theme="dangerous"
                            >
                                Удалить
                            </Button>
                        </div>
                    )}
                </StatusPopup>
            </PageWrapper>
        </>
    );
};

export default Product;
