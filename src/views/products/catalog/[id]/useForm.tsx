import { ProductDetail } from '@api/catalog/types/products';
import * as Yup from 'yup';

const useForm = ({ productData, images }: { productData?: ProductDetail; images: File[] }) => {
    const initialValues = {
        allow_publish: productData?.allow_publish || false,
        barcode: productData?.barcode || '',
        base_price: productData?.base_price || '',
        brand_id: productData?.brand_id || '',
        category_id: productData?.category_id || '',
        code: productData?.code || '',
        description: productData?.description || '',
        external_id: productData?.external_id || '',
        height: productData?.height || '',
        images: images || [],
        is_adult: productData?.is_adult || false,
        length: productData?.length || '',
        name: productData?.name || '',
        type: productData?.type || '',
        vendor_code: productData?.vendor_code || '',
        weight: productData?.weight || '',
        weight_gross: productData?.weight_gross || '',
        width: productData?.width || '',
        created_at: productData?.created_at || '',
        updated_at: productData?.updated_at || '',
    };

    const getFormValues = (values: typeof initialValues) => ({
        external_id: values.external_id,
        name: values.name,
        code: values.code,
        description: values.description,
        type: Number(values.type),
        allow_publish: values.allow_publish,
        vendor_code: values.vendor_code,
        barcode: String(values.barcode),
        weight: Number(values.weight) || undefined,
        weight_gross: Number(values.weight_gross) || undefined,
        length: Number(values.length) || undefined,
        width: Number(values.width) || undefined,
        height: Number(values.height) || undefined,
        is_adult: values.is_adult,
        base_price: Number(values.base_price) || undefined,
    });

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Обязательное поле'),
        vendor_code: Yup.string().required('Обязательное поле'),
        barcode: Yup.number()
            .nullable()
            .required('Обязательное поле')
            .test('barcode-range', 'Числовое значения должно быть от 8 до 13 символов', val => {
                if (val) {
                    const value = val.toString();
                    return value.length >= 8 && value.length <= 13;
                }
                return false;
            }),
        base_price: Yup.number().required('Обязательное поле'),
        type: Yup.number().nullable().required('Обязательное поле'),
        /** TODO: Если есть необходимость, то провалидировать необязательные поля */
        // widht: Yup.number(),
        // lenght: Yup.number(),
        // allow_publish: Yup.boolean(),
        // is_adult: Yup.boolean(),
    });

    return { initialValues, validationSchema, getFormValues };
};

export default useForm;
