import { useMemo, useState } from 'react';
import { useRouter } from 'next/router';
import { Button, typography } from '@scripts/gds';
import { useProducts, useProductsTypes } from '@api/catalog';

import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';

import { declOfNum } from '@scripts/helpers';
import { ITEMS_PER_PRODUCTS_PAGE } from '@scripts/constants';
import { useFiltersHelper } from '@scripts/hooks';

import Table, { Data, TableEmpty, TableFooter, TableHeader } from '@components/Table';
import PlusIcon from '@icons/plus.svg';
import CatalogFilter from './Filter';
import useTable from './useTable';

const Catalog = () => {
    const { pathname, push, query } = useRouter();
    const activePage = Number(query?.page || 1);
    const [itemsPerPageCount, setItemsPerPageCount] = useState(ITEMS_PER_PRODUCTS_PAGE);

    const { columns } = useTable();

    const emptyInitialValues = {
        id: '',
        name: '',
        code: '',
        barcode: '',
        vendor_code: '',
        type: '',
        is_adult: '',
        priceFrom: '',
        priceTo: '',
        allow_publish: '',
        created_at_lte: '',
        created_at_gte: '',
        updated_at_lte: '',
        updated_at_gte: '',
    };

    const { initialValues, filtersActive, URLHelper } = useFiltersHelper(emptyInitialValues);

    const { data, isLoading } = useProducts({
        filter: {
            id: initialValues.id || null,
            name: initialValues.name,
            code: initialValues.code || null,
            vendor_code: initialValues.vendor_code || null,
            type: initialValues.type || null,
            // eslint-disable-next-line no-nested-ternary
            is_adult: initialValues.is_adult || null,
            allow_publish: initialValues.allow_publish || null,
            created_at_lte: initialValues.created_at_lte ? new Date(initialValues.created_at_lte) : null,
            created_at_gte: initialValues.created_at_gte ? new Date(initialValues.created_at_gte) : null,
        },
        pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
    });

    const { data: apiTypes } = useProductsTypes();

    const convertedProducts = useMemo<Data[]>(
        () =>
            data?.data?.map(el => ({
                id: el.id || '-',
                name: el.name || '-',
                code: el.code || '-',
                img: el.images ? el.images[0].url : '-',
                vendor_code: el.vendor_code || '-',
                type: apiTypes?.data.find(elem => elem.id === el.type)?.name || '-',
                is_adult: el.is_adult ? 'Да' : 'Нет',
                base_price: el.base_price ? el.base_price : '-',
                activity: '',
                created_at: el.created_at || '-',
                updated_at: el.updated_at || '-',
                allow_publish: el.allow_publish,
                barcode: el.barcode ? el.barcode : '-',
                main_image: el?.main_image || '',
            })) || [],
        [apiTypes?.data, data?.data]
    );

    const totalProducts = data?.meta?.pagination?.total || 0;

    const pages = Math.ceil(totalProducts / itemsPerPageCount);

    const Header = () => (
        <TableHeader css={{ paddingLeft: 0, paddingRight: 0 }}>
            <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                    Найдено {`${totalProducts} ${declOfNum(totalProducts, ['товар', 'товара', 'товаров'])}`}
                </p>
                <Button Icon={PlusIcon} theme="primary" type="submit" onClick={() => push(`${pathname}/new`)}>
                    Создать товар
                </Button>
            </div>
        </TableHeader>
    );

    return (
        <PageWrapper h1="Товары" isLoading={isLoading}>
            <>
                <CatalogFilter
                    apiTypes={apiTypes}
                    initialValues={initialValues}
                    onSubmit={URLHelper}
                    onReset={() => push(pathname)}
                />
                <Block>
                    <Block.Body>
                        <Table renderHeader={Header} columns={columns} data={convertedProducts} />
                        {convertedProducts.length === 0 ? (
                            <TableEmpty
                                filtersActive={filtersActive}
                                titleWithFilters="Товары с данными фильтрами не найдены"
                                titleWithoutFilters="Товары отсутствуют"
                            />
                        ) : (
                            <TableFooter
                                pages={pages}
                                itemsPerPageCount={itemsPerPageCount}
                                setItemsPerPageCount={setItemsPerPageCount}
                            />
                        )}
                    </Block.Body>
                </Block>
            </>
        </PageWrapper>
    );
};

export default Catalog;
