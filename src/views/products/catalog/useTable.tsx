import { Data, getSelectColumn, getSettingsColumn, Cell } from '@components/Table';
import Link from 'next/link';
import { Column } from 'react-table';
import Circle from '@components/Circle';
import { useLinkCSS } from '@scripts/hooks';
import { useTheme, scale } from '@scripts/gds';

const useTable = () => {
    const linkStyles = useLinkCSS();
    const { colors } = useTheme();
    const columns: Column<Data>[] = [
        getSelectColumn(),
        {
            Header: 'ID',
            accessor: 'id',
        },
        {
            Header: 'Названия товаров',
            accessor: 'name',
            Cell: ({ row }) => (
                <Link href={`/products/catalog/${row.original.id}`} passHref>
                    <a css={linkStyles}>{row.original.name}</a>
                </Link>
            ),
        },
        {
            Header: 'Штрихкод',
            accessor: 'barcode',
        },
        {
            Header: 'Код товара',
            accessor: 'code',
        },
        {
            Header: 'Изображение',
            accessor: 'main_image',
            Cell: ({ value }) => {
                console.log(value);
                return <Cell value={value} type="photo" />;
                // if (value.length > 0) return <Image width={60} height={40} src={value} />;
                // return <TableImgPreview width={60} height={40} />;
            },
        },
        {
            Header: 'Артикул',
            accessor: 'vendor_code',
        },
        {
            Header: 'Тип товара',
            accessor: 'type',
        },
        {
            Header: 'Товар 18+',
            accessor: 'is_adult',
        },
        {
            Header: 'Стоимость',
            accessor: 'base_price',
        },
        {
            Header: 'Активность на витрине',
            accessor: 'allow_publish',
            Cell: ({ value }) =>
                value ? (
                    <div>
                        <Circle css={{ background: colors?.success, marginRight: scale(1) }} />
                        <span css={{ verticalAlign: 'middle' }}>Aктивен</span>
                    </div>
                ) : (
                    <div>
                        <Circle css={{ background: colors?.danger, marginRight: scale(1) }} />
                        <span css={{ verticalAlign: 'middle' }}>Не активен</span>
                    </div>
                ),
        },
        {
            Header: 'Дата создания',
            accessor: 'created_at',
            Cell: ({ value }) => <Cell value={value} type="datetime" />,
        },
        {
            Header: 'Дата обновления',
            accessor: 'updated_at',
            Cell: ({ value }) => <Cell value={value} type="datetime" />,
        },
        getSettingsColumn(),
    ];
    return { columns };
};

export default useTable;
